import { Component, OnInit } from '@angular/core';
import {SessionStorageService} from 'ngx-webstorage';
import {DialogComponent} from '../common/dialog/dialog.component';
import {ServicesClass} from '../services/service';
import {NewsComponent} from '../news/news.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  date = '06 Apr, 2020';
  referralCode = 'qtradeai.com/';
  user = this.SessionSt.retrieve('userData');
  picUrl = 'https://apis.qtradeai.com/public/';
  newsData: any;
  broker: any;
  referrals: any;
  details: any = [];
  car = '0';
  currency = this.SessionSt.retrieve('currency');
  ord: any;
  _gei : any;
  tdate = '06 Apr, 2020';

  constructor(
      private SessionSt: SessionStorageService,
      private dialog: DialogComponent,
      private service: ServicesClass,
      private news: NewsComponent
  ) {
    this.activities();
  }

  ngOnInit() {
    this.service.runDetails().then((data) => {
      if (data) {
        this.details = data;
        this.ord = data[0].orders;
        this.doGEI(this.ord);
      } else {}}
    ).catch(e => {});

    this.news.fetchNews((data) => {
      if (data.content.data != null || undefined) {
        this.newsData = data.content.data[0];
      }
    });

  }

  doGEI( orders: any ) {
    this.service.getOrders().subscribe((data) => {
      if (data) {
        const result = data.content.data.find(( {gei} ) => gei === '1');
        if (result && result.status !== '0') {
          this._gei = result;
          const d = result.transDate;
          const t = Number(result.grace);
          let date = new Date(d * 1000); // Now
          date.setDate(date.getDate() + t); // Set now + 30 days as the new date
          const newDate = new Date(date).toLocaleDateString('en-GB', {
            month: 'long',
            day: 'numeric',
            year: 'numeric'
          });
          this.date = newDate;

          const q = Number(result.maturity);
          let date1 = new Date(d * 1000); // Now
          date1.setDate(date1.getDate() + q); // Set now + 30 days as the new date
          const newDate1 = new Date(date1).toLocaleDateString('en-GB', {
            month: 'long',
            day: 'numeric',
            year: 'numeric'
          });
          this.tdate = newDate1;

        } else {
          // this._gei = 0;
        }
        }
    });
  }

  copy(username) {
    try {
      if ((navigator as any).clipboard) {
        (navigator as any).clipboard.writeText(this.referralCode + username);
      } else if ((window as any).clipboardData) {  // IE
        (window as any).clipboardData.setData('text', this.referralCode + username);
      } else {  // other browsers, iOS, Mac OS
        document.execCommand('copy', false, this.referralCode + username);
      }
      this.service.toast('info', 'Copied to Clipboard.', 'Copied.'); // copy succeed.
    } catch (e) {
      this.service.toast('danger', 'could not copy referral link', 'Copy Failed'); // copy failed.
    }
  }

  fetchBroker(cb) {
    const req = new XMLHttpRequest();
    req.open('GET', this.service.rootUrl + '/customer/get/broker');
    req.setRequestHeader('username', this.SessionSt.retrieve('username'));
    req.setRequestHeader('publicKey', this.SessionSt.retrieve('publicKey'));
    req.setRequestHeader('userID', this.SessionSt.retrieve('userID'));

    req.onload = () => {
      cb(JSON.parse(req.response));
    };

    req.send();
  }

  fetchReferals(cb) {
    const req = new XMLHttpRequest();
    req.open('GET', this.service.rootUrl + '/customer/get/referrals');
    req.setRequestHeader('username', this.SessionSt.retrieve('username'));
    req.setRequestHeader('publicKey', this.SessionSt.retrieve('publicKey'));
    req.setRequestHeader('userID', this.SessionSt.retrieve('userID'));

    req.onload = () => {
      cb(JSON.parse(req.response));
    };

    req.send();
  }

  async activities() {
    await this.fetchBroker((data: any) => {
      if (data.content) {
        return this.broker = data.content.data;
      }
    });

    await this.fetchReferals((data: any) => {
      if (data.content) {
        return this.referrals = data.content.data;
      }
    });

  }

  showDialog(e, h) {
    this.SessionSt.store('modal', {e, h});
    this.dialog.showModal('commitmentMethod');
    // this.dialog.showDialog();
  }

}
