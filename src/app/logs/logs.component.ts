import {AfterViewInit, Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {SessionStorageService} from 'ngx-webstorage';
import {DialogComponent} from '../common/dialog/dialog.component';
import {ServicesClass} from '../services/service';

@Component({
    selector: 'app-logs',
    templateUrl: './logs.component.html',
    styleUrls: ['./logs.component.scss']
})
export class LogsComponent implements OnInit, OnDestroy {

    orderRow;
    orderColumns;
    withdrawRow;
    withdrawColumns;
    transferRow;
    transferColumns;
    paymentRow;
    paymentColumns;
    region = this.SessionSt.retrieve('currency');
    readonly rootUrl = 'https://apis.qtradeai.com/public/api';
    readThis: any;
    changeListener: any;
    image: any;
    details: any = [];

    // No Option YET
    // https://github.com/swimlane/ngx-datatable/issues/423
    scrollBarHorizontal = (window.innerWidth < 960);
    columnModeSetting = (window.innerWidth < 960) ? 'standard' : 'force';
    columnModeSettingSmall = (window.innerWidth < 560) ? 'standard' : 'force';
    @ViewChild('tableAction') tableAction: TemplateRef<any>;
    @ViewChild('paymentAction') paymentAction: TemplateRef<any>;
    @ViewChild('dateColumn') dateColumn: TemplateRef<any>;
    @ViewChild('dateColumnP') dateColumnP: TemplateRef<any>;
    @ViewChild('dateColumnT') dateColumnT: TemplateRef<any>;
    @ViewChild('dateColumnW1') dateColumnW1: TemplateRef<any>;
    @ViewChild('dateColumnW2') dateColumnW2: TemplateRef<any>;
    @ViewChild('costColumn') costColumn: TemplateRef<any>;
    @ViewChild('costColumnP') costColumnP: TemplateRef<any>;
    @ViewChild('costColumnT') costColumnT: TemplateRef<any>;
    @ViewChild('costColumnW1') costColumnW1: TemplateRef<any>;
    @ViewChild('costColumnW2') costColumnW2: TemplateRef<any>;
    @ViewChild('charge') charge: TemplateRef<any>;
    @ViewChild('statusColumn') statusColumn: TemplateRef<any>;
    @ViewChild('statusColumnP') statusColumnP: TemplateRef<any>;
    @ViewChild('statusColumnT') statusColumnT: TemplateRef<any>;
    @ViewChild('statusColumnW') statusColumnW: TemplateRef<any>;
    @ViewChild('methodColumnP') methodColumnP: TemplateRef<any>;
    @ViewChild('typeColumnT') typeColumnT: TemplateRef<any>;

    constructor(
        private SessionSt: SessionStorageService,
        private dialog: DialogComponent,
        private service: ServicesClass
    ) {
        this.service.getOrders().subscribe((data) => {
            if (data.content.data != null || undefined) {
                this.orderRow = data.content.data;
                this.withdrawRow = data.content.data;
            }
            this.orderColumns = [
                {name: 'id', prop: 'orderID'},
                {name: 'package', prop: 'packageName'},
                {name: 'amount', prop: 'orderCount'},
                {name: 'status', prop: 'status', cellTemplate: this.statusColumn},
                {name: 'cost', prop: 'packagePrice', cellTemplate: this.costColumn},
                {name: 'date', prop: 'transDate', cellTemplate: this.dateColumn},
                {prop: 'actions', name: 'Actions', cellTemplate: this.tableAction}
            ];
        });

        this.service.getWithdrawal().subscribe((data) => {
            if (data.content.data != null || undefined) {
                this.withdrawRow = data.content.data;
            }
            this.withdrawColumns = [
                {name: 'id', prop: '_id'},
                {name: 'Acc Name', prop: 'accountName'},
                {name: 'Acc Number', prop: 'accountNumber'},
                {name: 'status', prop: 'status', cellTemplate: this.statusColumnW},
                {name: 'charge', prop: 'charge', cellTemplate: this.charge},
                {name: 'amount', prop: 'amount', cellTemplate: this.costColumnW2},
                {name: 'Trans date', prop: 'transDate', cellTemplate: this.dateColumnW1},
                {name: 'Comp date', prop: 'completedDate', cellTemplate: this.dateColumnW2},
            ];
        });

        this.service.getTransfers().subscribe((data) => {
            if (data.content.data != null || undefined) {
                this.transferRow = data.content.data;
            }
            this.transferColumns = [
                {name: 'id', prop: 'transID'},
                {name: 'sender', prop: 'fromAccount'},
                {name: 'beneficiary', prop: 'toAccount'},
                {name: 'status', prop: 'status', cellTemplate: this.statusColumnT},
                {name: 'amount', prop: 'amount', cellTemplate: this.costColumnT},
                {name: 'type', prop: 'type', cellTemplate: this.typeColumnT},
                {name: 'date', prop: 'transDate', cellTemplate: this.dateColumnT},
            ];
        });

        this.service.getPayment().subscribe((data) => {
            if (data.content.data != null || undefined) {
                this.paymentRow = data.content.data;
            }
            this.paymentColumns = [
                {name: 'id', prop: 'orderID'},
                {name: 'method', prop: 'method', cellTemplate: this.methodColumnP},
                {name: 'status', prop: 'status', cellTemplate: this.statusColumnP},
                {name: 'amount', prop: 'amount', cellTemplate: this.costColumnP},
                {name: 'payment ID', prop: '_id'},
                {name: 'date', prop: 'transDate', cellTemplate: this.dateColumnP},
                {prop: 'actions', name: 'actions', cellTemplate: this.paymentAction}
            ];
        });

        window.onresize = () => {
            this.scrollBarHorizontal = (window.innerWidth < 960);
            this.columnModeSetting = (window.innerWidth < 960) ? 'standard' : 'force';
            this.columnModeSettingSmall = (window.innerWidth < 560) ? 'standard' : 'force';
        };
    }

    selected = [];
    currency = this.SessionSt.retrieve('currency');
    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }

    onActivate(event) {
    }

    setClass(order, t) {
        let Objclass = '';
        let labelText = '';
        if (order === '0') {
            Objclass = 'label label-warning';
            labelText = 'Awaiting Payment';
        }else if (order === '-1') {
            Objclass = 'label label-danger';
            labelText = 'Cancelled';
        }else if (order === '1') {
            Objclass = 'label label-primary';
            labelText = 'Paid';
        }else if (order === '2') {
            Objclass = 'label label-inverse';
            labelText = 'Shipped';
        }else if (order === '3') {
            Objclass = 'label label-custom';
            labelText = 'Delivered';
        }else if (order === '10') {
            Objclass = 'label label-success';
            labelText = 'Delivered';
        }
        return t === 'c' ? Objclass : labelText;
    }

    setClassw(withdraw, t) {
        let Objclass = '';
        let labelText = '';
        if (withdraw === '0') {
            Objclass = 'label label-warning';
            labelText = 'Pending';
        }else if (withdraw === '-1') {
            Objclass = 'label label-danger';
            labelText = 'Failed';
        }else if (withdraw === '1') {
            Objclass = 'label label-success';
            labelText = 'Paid';
        }else if (withdraw === '2') {
            Objclass = 'label label-inverse';
            labelText = 'Confirmed';
        }
        return t === 'c' ? Objclass : labelText;
    }

    setClasst(transfer, t) {
        let Objclass = '';
        let labelText = '';
        if (transfer === '-1') {
            Objclass = 'label label-danger';
            labelText = 'Failed';
        }else if (transfer === '1') {
            Objclass = 'label label-primary';
            labelText = 'Success';
        }
        return t === 'c' ? Objclass : labelText;
    }

    setClassty(transfer, t) {
        let Objclass = '';
        let labelText = '';
        if (transfer === this.SessionSt.retrieve('userID')) {
            Objclass = 'label label-success';
            labelText = 'sent';
        }else {
            Objclass = 'label label-inverse';
            labelText = 'received';
        }
        return t === 'c' ? Objclass : labelText;
    }

    setClasspm(payment, t) {
        let Objclass = '';
        let labelText = '';
        if (payment === '1') {
            Objclass = 'label label-danger';
            labelText = 'Wallet';
        }else if (payment === '2') {
            Objclass = 'label label-primary';
            labelText = 'Card';
        }else if (payment === '3') {
            Objclass = 'label label-primary';
            labelText = 'Transfer/Deposit';
        }
        return t === 'c' ? Objclass : labelText;
    }

    setClassps(payment, t) {
        let Objclass = '';
        let labelText = '';
        if (payment === '-1') {
            Objclass = 'label label-danger';
            labelText = 'Failed';
        }else if (payment === '0') {
            Objclass = 'label label-warning';
            labelText = 'Pending';
        }else if (payment === '1') {
            Objclass = 'label label-success';
            labelText = 'Successful';
        }
        return t === 'c' ? Objclass : labelText;
    }

    showDialog(e, h) {
        this.SessionSt.store('modal', {e, h});
        this.dialog.showDialog();
    }

    upload() {
        const q = document.getElementById('input-file-id');
        q.click();
    }


    ngOnInit() {
        this.service.runDetails().then((data) => {
            if (data) {
                this.details = data;
            } else {}}
        ).catch(e => 
            // console.log(e)
            {});

        this.changeListener = function($event, payment) {
            this.readThis($event.target, payment);
        };

        this.readThis = function(inputValue: any, payment): void {
            const file: File = inputValue.files[0];
            const myReader: FileReader = new FileReader();

            myReader.onloadend = (e) => {
                this.image = myReader.result;
                // console.log(this.image);
                this.showDialog({a: payment, b: myReader.result}, 'uploadPayment');
            };
            myReader.readAsDataURL(file);
            return myReader.result;
        };

        const rp = sessionStorage.getItem('pstat');
        if (rp === 't') {
            this.service.toast('success', 'Payment Successful!', 'Success');
        }else if (rp === 'f') {
            this.service.toast('danger', 'Payment Failed!', 'Failed');
        }
        sessionStorage.removeItem('pstat');
    }

    ngOnDestroy(): void {
    }

}
