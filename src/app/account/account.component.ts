import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {SessionStorageService} from 'ngx-webstorage';
import {DialogComponent} from '../common/dialog/dialog.component';
import {ServicesClass} from '../services/service';
import {NgForm} from '@angular/forms';

@Component({
    selector: 'app-account',
    templateUrl: './account.component.html',
    styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {
    bankRow;
    banks = this.SessionSt.retrieve('banks');
    bankColumns;
    details: any = [];
    region = this.SessionSt.retrieve('region');
    currency = this.SessionSt.retrieve('currency');
    BMWalletAddress = this.SessionSt.retrieve('BMWalletAddress');
    userData = this.SessionSt.retrieve('userData');
    BMTaddress = this.SessionSt.retrieve('BMTaddress');
    BTCaddress = this.SessionSt.retrieve('BTCaddress');
    selected = [];
    cardColumns: any;
    cardRow: any;


    // No Option YET
    // https://github.com/swimlane/ngx-datatable/issues/423
    scrollBarHorizontal = (window.innerWidth < 960);
    columnModeSetting = (window.innerWidth < 960) ? 'standard' : 'force';
    columnModeSettingSmall = (window.innerWidth < 560) ? 'standard' : 'force';
    @ViewChild('tableAction') tableAction: TemplateRef<any>;
    @ViewChild('cardAction') cardAction: TemplateRef<any>;
    @ViewChild('dateColumn') dateColumn: TemplateRef<any>;
    @ViewChild('statusColumn') statusColumn: TemplateRef<any>;
    @ViewChild('bvnColumn') bvnColumn: TemplateRef<any>;
    @ViewChild('cardLimitColumn') cardLimitColumn: TemplateRef<any>;
    @ViewChild('panColumn') panColumn: TemplateRef<any>;

    constructor(
        private SessionSt: SessionStorageService,
        private dialog: DialogComponent,
        private service: ServicesClass
    ) {
        this.service.getMyBank().subscribe((data) => {
            if (data.content.data != null || undefined) {
                this.bankRow = data.content.data;
                if (data.content.data.length === 1) {
                    this.SessionSt.store('bank', '1');
                    this.SessionSt.store('myBanks', data.content.data);
                } else {
                    this.SessionSt.store('bank', '0');
                }
            }
            if (this.region === 'Africa') {
                this.bankColumns = [
                    {name: 'Bank Name', prop: 'bankName'},
                    {name: 'Account Number', prop: 'accountNumber'},
                    {name: 'Account Name', prop: 'accountName'},
                    {prop: 'actions', name: 'Actions', cellTemplate: this.tableAction}
                ];
            } else {
                this.bankColumns = [
                    {name: 'Account Number', prop: 'accountNumber'},
                    {name: 'Account Name', prop: 'accountName'},
                    {name: 'Sort Code', prop: 'sortCode'},
                    {name: 'Swift Code', prop: 'swiftCode'},
                    {name: 'Bank Address', prop: 'bankAddress'},
                    {prop: 'actions', name: 'Actions', cellTemplate: this.tableAction}
                ];
            }
        });

        this.service.getCards().subscribe((data) => {
            this.cardColumns = [
                {name: 'ID', prop: 'requestID'},
                {name: 'Pan', prop: 'pan', cellTemplate: this.panColumn},
                {name: 'Status', prop: 'status', cellTemplate: this.statusColumn},
                {name: 'BVN', prop: 'bvn', cellTemplate: this.bvnColumn},
                {name: 'Card Limit', prop: 'cardLimit', cellTemplate: this.cardLimitColumn},
                {name: 'Date', prop: 'transDate', cellTemplate: this.dateColumn},
                {prop: 'actions', name: 'Actions', cellTemplate: this.cardAction}
            ];
            if (data.content.data != null || undefined) {
                this.cardRow = data.content.data;
            }
        });
    }

    onSelect({ selected }) {
        this.selected.splice(0, this.selected.length);
        this.selected.push(...selected);
    }

    onActivate(event) {
    }

    showDialog(e, h) {
        this.SessionSt.store('modal', {e, h});
        this.dialog.showDialog();
    }


    ngOnInit(): void {
        this.service.runDetails().then((data) => {
            if (data) {
                this.details = data;
            } else {}}
        ).catch(e => console.log(e));

        console.log(this.region);
    }

    setClassps(payment, t) {
        let Objclass = '';
        let labelText = '';
        if (payment === '-1') {
            Objclass = 'label label-danger';
            labelText = 'Failed';
        }else if (payment === '0') {
            Objclass = 'label label-warning';
            labelText = 'Pending';
        }else if (payment === '1') {
            Objclass = 'label label-success';
            labelText = 'Successful';
        }
        return t === 'c' ? Objclass : labelText;
    }

    showModal(e) {
        this.dialog.showModal(e);
    }

    SaveBMCT(BMCTForm: NgForm) {
        const updateItem = this.userData;

        const q = {...updateItem, ...BMCTForm.value};

        this.service.updateSelf(q)
            .subscribe((data) => {
                if (data.error.status === '0') {
                    this.service.toast('success', 'Address saved Successful', 'Success');
                    this.service.runDetails();
                    this.SessionSt.store('BMWalletAddress', BMCTForm.value.BMCTaddress);
                } else {
                    this.service.toast('danger', data.error.message, 'Failed');
                }
            });
    }

    checkValidity( value ) {
        if (value !== '' || null) {
            return value;
        }

        return '-';
    }
}
