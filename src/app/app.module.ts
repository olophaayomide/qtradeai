// Angular Core
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule,  HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule} from '@angular/http';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { RouterModule } from '@angular/router';

// Routing
import { AppRoutes } from './app.routing';
import { AppComponent } from './app.component';

// Layouts
import {RootLayout, Home, BlankComponent} from './@pages/layouts';
// Layout Service - Required
import { pagesToggleService } from './@pages/services/toggler.service';

// Shared Layout Components
import { SidebarComponent } from './@pages/components/sidebar/sidebar.component';
import { QuickviewComponent } from './@pages/components/quickview/quickview.component';
import { QuickviewService } from './@pages/components/quickview/quickview.service';
import { SearchOverlayComponent } from './@pages/components/search-overlay/search-overlay.component';
import { HeaderComponent } from './@pages/components/header/header.component';
import { HorizontalMenuComponent } from './@pages/components/horizontal-menu/horizontal-menu.component';
import { SharedModule } from './@pages/components/shared.module';
import { pgListViewModule} from './@pages/components/list-view/list-view.module';
import { pgCardModule} from './@pages/components/card/card.module';
import { pgCardSocialModule} from './@pages/components/card-social/card-social.module';

// Basic Bootstrap Modules
import {BsDropdownModule,
        AccordionModule,
        AlertModule,
        ButtonsModule,
        CollapseModule,
        ModalModule,
        ProgressbarModule,
        TabsModule,
        TooltipModule,
        TypeaheadModule,
} from 'ngx-bootstrap';

// Pages Globaly required Components - Optional
import { pgTabsModule } from './@pages/components/tabs/tabs.module';
import { pgSwitchModule } from './@pages/components/switch/switch.module';
import { ProgressModule } from './@pages/components/progress/progress.module';

// Thirdparty Components / Plugins - Optional
import { QuillModule } from 'ngx-quill';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import {pgSelectfx} from './@pages/components/cs-select/select.module';
import {pgSelectModule} from './@pages/components/select/select.module';
import {LogsComponent} from './logs/logs.component';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {Ng2Webstorage} from 'ngx-webstorage';
import {MessageService} from './@pages/components/message/message.service';
import {MessageModule} from './@pages/components/message/message.module';
import {DialogComponent} from './common/dialog/dialog.component';
import {JwtInterceptor} from './services';
import {AccountComponent} from './account/account.component';
import {PackagesComponent} from './packages/packages.component';
import {NewsComponent} from './news/news.component';
import {AuthGuard} from './services/authGuard';
import {DashboardComponent} from './dashboard/dashboard.component';
import {CarBonusComponent} from './common/car-bonus/car-bonus.component';
import {QuickStatsWidgetComponent} from './common/quick-stats-widget/quick-stats-widget.component';
import {TableBasicWidgetComponent} from './common/table-basic-widget/table-basic-widget.component';
import {GeiPackagesComponent} from './geipackages/gei-packages.component';
import {AbbreviationPipe} from './pipe/abbreviation.pipe';
import {SessionGuard} from './services/sessionGuard';
import {ServicesClass} from './services/service';
import {ReferralComponent} from './referrals/referral.component';
import {ProfileComponent} from './profile/profile.component';
import {pgUploadModule} from './@pages/components/upload/upload.module';
import { StokistComponent } from './stokists/stokists.component';
import { BeepEstateComponent } from './beepestate/beepestate.component';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

// Hammer Config Overide
// https://github.com/angular/angular/issues/10541
export class AppHammerConfig extends HammerGestureConfig  {
  overrides = <any>{
      'pinch': { enable: false },
      'rotate': { enable: false }
  };
}

@NgModule({
  declarations: [
    AppComponent,
    Home,
    BlankComponent,
    SidebarComponent, QuickviewComponent, SearchOverlayComponent, HeaderComponent, HorizontalMenuComponent, DialogComponent,
    RootLayout,
    LogsComponent,
    AccountComponent,
    PackagesComponent,
    GeiPackagesComponent,
    NewsComponent,
    DashboardComponent,
    CarBonusComponent,
    QuickStatsWidgetComponent,
    TableBasicWidgetComponent,
    AbbreviationPipe,
    ReferralComponent,
    ProfileComponent,
    StokistComponent,
    BeepEstateComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    SharedModule,
    ProgressModule,
    pgListViewModule,
    pgSelectModule,
    pgCardModule,
    pgCardSocialModule,
    RouterModule.forRoot(AppRoutes),
    BsDropdownModule.forRoot(),
    AccordionModule.forRoot(),
    AlertModule.forRoot(),
    ButtonsModule.forRoot(),
    CollapseModule.forRoot(),
    ModalModule.forRoot(),
    ProgressbarModule.forRoot(),
    TabsModule.forRoot(),
    TooltipModule.forRoot(),
    TypeaheadModule.forRoot(),
    pgTabsModule,
    pgSelectfx,
    PerfectScrollbarModule,
    pgSwitchModule,
    QuillModule,
    NgxDatatableModule,
    Ng2Webstorage,
    MessageModule,
    pgUploadModule,
  ],
  providers: [
      QuickviewService, ServicesClass, pagesToggleService, MessageService, DialogComponent, AuthGuard, SessionGuard, NewsComponent, {
    provide: PERFECT_SCROLLBAR_CONFIG,
    useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
  },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
  {
    provide: HAMMER_GESTURE_CONFIG,
    useClass: AppHammerConfig
  }],
  bootstrap: [AppComponent],
})
export class AppModule { }
