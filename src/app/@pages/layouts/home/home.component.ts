import {Component, Inject, OnInit, ViewEncapsulation} from '@angular/core';
import { RootLayout } from '../root/root.component';
import {SessionStorageService} from 'ngx-webstorage';
import {pagesToggleService} from '../../services/toggler.service';
import {Router} from '@angular/router';
import {ServicesClass} from '../../../services/service';

@Component({
  selector: 'simplywhite-layout',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class Home extends RootLayout implements OnInit {
  menuLinks = [
    {
      label: 'Dashboard',
      details: 'Statistics/Activities',
      routerLink: '/app',
      iconType: 'fi',
      iconName: 'shield',
    },
    {
      label: 'News',
      routerLink: '/app/news',
      details: 'Get updated Info',
      iconType: 'fi',
      iconName: 'mail'
    },
    {
      label: 'Referrals',
      routerLink: '/app/referrals',
      details: 'View Referrals',
      iconType: 'fi',
      iconName: 'users'
    },
    {
      label: 'Order',
      routerLink: '/app/support-packages',
      details: 'list of subscriptions',
      iconType: 'fi',
      iconName: 'grid'
    },
    {
      label: 'GEI Packages',
      routerLink: '/app/gei-packages',
      details: 'list of subscriptions',
      iconType: 'fi',
      iconName: 'grid'
    },
    {
      label: 'Stockist',
      routerLink: '/app/stockist',
      details: 'list of stockist',
      iconType: 'fi',
      iconName: 'users'
    },
    {
      label: 'BeepEstate',
      routerLink: '/app/beepestate',
      details: 'list of beepestates',
      iconType: 'fi',
      iconName: 'triangle'
    },
    {
      label: 'Account',
      routerLink: '/app/account',
      details: 'Wallet & Addresses',
      iconType: 'fi',
      iconName: 'triangle'
    },
    {
      label: 'Logs',
      routerLink: '/app/logs',
      details: 'Transaction history',
      iconType: 'fi',
      iconName: 'list'
    },
    {
      label: 'Profile',
      routerLink: '/app/profile',
      details: 'Personal Information',
      iconType: 'fi',
      iconName: 'settings'
    },
];

  user = this.SessionSt.retrieve('userData');
  picUrl = 'https://apis.qtradeai.com/public/';



  // tslint:disable-next-line:max-line-length
  constructor( a: pagesToggleService, b: Router, public SessionSt: SessionStorageService, public service: ServicesClass) {
    super(a, b);
  }

  logout() {
    this.service.logout();
  }

  ngOnInit() {
    this.changeLayout('menu-pin');
    // Will sidebar close on screens below 1024
    this.autoHideMenuPin();
  }

}
