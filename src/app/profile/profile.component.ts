import {Component, OnInit} from '@angular/core';
import {SessionStorageService} from 'ngx-webstorage';
import {DialogComponent} from '../common/dialog/dialog.component';
import {ServicesClass} from '../services/service';
import {UploadChangeParam} from '../@pages/components/upload/interface';
import {NgForm} from '@angular/forms';


@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
    readThis: any;
    userData = this.SessionSt.retrieve('userData');
    details: any;
    newPassword: any;
    Password: any;
    oldPassword: any;
    imageData: any;
    picUrl = this.service.picUrl;
    addressID: any;
    countryID: any;
    imageData2: any;
    pPic: any;

    constructor(
        public service: ServicesClass,
        private SessionSt: SessionStorageService,
        public dialog: DialogComponent
    ) {
    }


    ngOnInit() {
        this.service.runDetails().then((data) => {
            if (data) {
                this.details = data;
            } else {}}
        ).catch(e => console.log(e));

        // this.readThis = async function(inputValue: any, path) {
        //     const file: File = inputValue.files[0];
        //     const myReader: FileReader = new FileReader();
        //
        //     myReader.onloadend = (e) => {
        //         this.imageData = myReader.result;
        //         this.continue(path);
        //     };
        //     myReader.readAsDataURL(file);
        //     return myReader.result;
        // };
    }

    saveProfile( payload: NgForm ) {
        // console.log(payload.value);

        const updateItem = this.userData;

        const q = {...updateItem, ...payload.value};

        this.service.updateSelf(q)
            .subscribe((data) => {
                if (data.error.status === '0') {
                    this.service.toast('success', 'Profile Updated Successfully', 'Success');
                    this.service.runDetails();
                    this.SessionSt.store('userData', q);
                } else {
                    this.service.toast('danger', data.error.message, 'Failed');
                }
            });
    }

    savePassword( passwordForm: NgForm ) {
        this.service.changePassword(passwordForm.value)
            .subscribe((data) => {
                if (data.error.status === '0') {
                    this.service.toast('success', 'Password Updated Successfully', 'Success');
                } else {
                    this.service.toast('danger', data.error.message, 'Failed');
                }
            });
    }

    addressKyc($event) {
        const inputValue = $event.target;
        const file: File = inputValue.files[0];
        const myReader: FileReader = new FileReader();
        myReader.readAsDataURL(file);

        myReader.onloadend = (e) => {
            this.imageData = myReader.result;
        };
    }

    countryKyc($event) {
        const inputValue = $event.target;
        const file: File = inputValue.files[0];
        const myReader: FileReader = new FileReader();
        myReader.readAsDataURL(file);

        myReader.onloadend = (e) => {
            this.imageData2 = myReader.result;
        };
    }

    library( payload: string ) {
        const q = document.getElementById(payload);
        q.click();
    }

    submitKyc( kycForm: NgForm ) {
        const q = {
            addressID: this.imageData,
            countryID: this.imageData2
        };
        this.service.updateKYC(q)
            .subscribe((data) => {
                if (data.error.status === '0') {
                    this.service.toast('success', 'KYC Updated Successfully', 'Success');
                } else {
                    this.service.toast('danger', data.error.message, 'Failed');
                }
            });
    }

    profilePic( $event ) {
        const inputValue = $event.target;
        const file: File = inputValue.files[0];
        const myReader: FileReader = new FileReader();
        myReader.readAsDataURL(file);

        myReader.onloadend = (e) => {
            this.pPic = myReader.result;
            this.submitPic();
        };
    }

    submitPic() {
        this.service.updatePic(this.pPic)
            .subscribe((data) => {
                if (data.error.status === '0') {
                    this.service.toast('success', 'Image Updated Successfully', 'Success');
                    this.service.runDetails();
                } else {
                    this.service.toast('danger', data.error.message, 'Failed');
                }
            });
    }
}
