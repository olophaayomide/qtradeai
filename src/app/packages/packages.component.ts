import {Component, OnInit} from '@angular/core';
import {SessionStorageService} from 'ngx-webstorage';
import {DialogComponent} from '../common/dialog/dialog.component';
import {ServicesClass} from '../services/service';


@Component({
    selector: 'app-packages',
    templateUrl: './packages.component.html',
    styleUrls: ['./packages.component.scss']
})
export class PackagesComponent implements OnInit {
    private packages: any;
    noPackage = false;

    constructor(
        private SessionSt: SessionStorageService,
        private dialog: DialogComponent,
        private service: ServicesClass
    ) {
    }


    ngOnInit() {
        this.service.getPackages().subscribe((data) => {
            var payload = [];
            if (data.content.data != null || undefined) {
                const q = data.content.data;
                for (let i = 0; i < q.length; i++) {
                    if (q[i].gei === '0') {
                        payload.push(q[i]);
                    }
                }

                this.packages = payload;
            }
            if(payload.length !== 0) {
                this.noPackage = true;
            }
        });
    }

    subscribe = async function( payload: any ) {
        // this.dialog.showModal('walletTransfer');
        this.SessionSt.store('packageName', payload.name);
        await this.service.subscribeOrder(payload)
            .subscribe((data) => {
                if (data.error.status === '0') {
        this.id = data.success.orderID;
        this.amount = data.success.amount;
        const q = {
            orderID: data.success.orderID,
            amount: data.success.amount
        };
        const h = 'continuePayment';
        this.SessionSt.store('modal', {q, h});
                    this.dialog.showModal('subscribeMethod');
                } else {
                    this.service.toast('danger', data.error.message, 'Failed');
                }
            });
    };
}
