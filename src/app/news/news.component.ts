import {Component, OnDestroy, OnInit} from '@angular/core';
import {pagesToggleService} from '../@pages/services/toggler.service';
import {PerfectScrollbarConfigInterface} from 'ngx-perfect-scrollbar';
import {List} from '../model/list';
import {ServicesClass} from '../services/service';
import {SessionStorageService} from 'ngx-webstorage';

declare var pg: any;
@Component({
    selector: 'app-news',
    templateUrl: './news.component.html',
    styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit, OnDestroy {
    emailList;
    isMobile = pg.getUserAgent() === 'mobile';
    public config: PerfectScrollbarConfigInterface = {};
    selectedEmail: List;
    subscription;
    timeout;
    isEmailSelected = false;

    constructor(
        private toggler: pagesToggleService,
        private service: ServicesClass,
        private SessionSt: SessionStorageService
    ) {
        this.fetchNews((data) => {
            if (data.content.data != null || undefined) {
                this.emailList = data.content.data;
            }
        });
    }


    ngOnInit() {
        this.service.runDetails().then((data) => {
            if (data) {
            } else {}}
        ).catch(e => {/**/});

        this.timeout = setTimeout(() => {
            this.toggler.toggleFooter(false);
        });

        this.toggler.setPageContainer('full-height');
        this.toggler.setContent('full-height');

    }
    ngOnDestroy() {
        // this.subscription.unsubscribe();
        clearTimeout(this.timeout);
    }
    onSelect(item: List): void {
        this.selectedEmail = item;
        this.isEmailSelected = true;
    }
    onBack() {
        this.isEmailSelected = false;
    }

    fetchNews(cb) {
        const req = new XMLHttpRequest();
        req.open('GET', this.service.rootUrl + '/customer/get/news/0000000000000/99999999999999999999');
        req.setRequestHeader('username', this.SessionSt.retrieve('username'));
        req.setRequestHeader('publicKey', this.SessionSt.retrieve('publicKey'));
        req.setRequestHeader('userID', this.SessionSt.retrieve('userID'));
        // req.open('GET', `assets/data/table.json`);

        req.onload = () => {
            cb(JSON.parse(req.response));
        };

        req.send();
    }

    reload () {
        this.fetchNews((data) => {
            if (data.content.data != null || undefined) {
                this.emailList = data.content.data;
            }
        });
    }

}
