import {AfterViewInit, Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {SessionStorageService} from 'ngx-webstorage';
import {DialogComponent} from '../common/dialog/dialog.component';
import {ServicesClass} from '../services/service';
import {DatatableComponent} from '@swimlane/ngx-datatable';

@Component({
    selector: 'app-stokist',
    templateUrl: './stokists.component.html',
    styleUrls: ['./stokists.component.scss']
})
export class StokistComponent implements OnInit, OnDestroy {
    levelRow: any;
    picUrl = this.service.picUrl;

    ref1 = [];
    ref1Sort = [];
    @ViewChild(DatatableComponent) table: DatatableComponent;
    ref5 = [];
    ref5Sort = [];
    @ViewChild(DatatableComponent) table5: DatatableComponent;
    ref4 = [];
    ref4Sort = [];
    @ViewChild(DatatableComponent) table4: DatatableComponent;
    ref3 = [];
    ref3Sort = [];
    @ViewChild(DatatableComponent) table3: DatatableComponent;
    ref2 = [];
    ref2Sort = [];
    @ViewChild(DatatableComponent) table2: DatatableComponent;
    active5: any;
    active4: any;
    active3: any;
    active2: any;
    active1: any;
    refColumn = [
        { name: 'area' },
        { name: 'name' },
        { name: 'Phone' },
        { name: 'Email' },
        { name: 'address' },
    ];

    // No Option YET
    // https://github.com/swimlane/ngx-datatable/issues/423
    scrollBarHorizontal = (window.innerWidth < 960);
    columnModeSetting = (window.innerWidth < 960) ? 'standard' : 'force';
    columnModeSettingSmall = (window.innerWidth < 560) ? 'standard' : 'force';

    @ViewChild('picColumn1') picColumn1: TemplateRef<any>;

    states = [
        { id: 'disabled', name: 'No Data', disabled: true }
    ];
    setState = '-';
    lg = [
        { id: 'disabled', name: 'Select State', disabled: true }
    ];
    setLg = '-';
    setArea: any;
    area = [
        { id: 'disabled', name: 'No Data', disabled: true }
    ];
    setStokist: any;
    stokist = [
        { id: 'disabled', name: 'No Data', disabled: true }
    ];

    constructor(
        private SessionSt: SessionStorageService,
        private dialog: DialogComponent,
        private service: ServicesClass
    ) {

    }

    ngOnDestroy(): void {
    }

    ngOnInit(): void {
        this.service.getState()
        .subscribe(
            (data: any) => {
              this.states = data.content.data;
            },
            (error) => {}
        );

        this.getStockist();

        

        // this.service.getReferrals().subscribe((data: any = []) => {
        //     this.ref1Sort = [...data.content.data.levelOne];
        //     this.ref1 = [...data.content.data.levelOne];
        //     this.ref2 = [...data.content.data.levelTwo];
        //     this.ref3 = [...data.content.data.levelThree];
        //     this.ref4 = [...data.content.data.levelFour];
        //     this.ref5 = [...data.content.data.levelFive];
        // });

        // this.service.getActiveReferrals().subscribe((data: any = []) => {
        //     this.active1 = data.content.data.levelOne[0].totalActive;
        //     this.active2 = data.content.data.levelTwo[0].totalActive;
        //     this.active3 = data.content.data.levelThree[0].totalActive;
        //     this.active4 = data.content.data.levelFour[0].totalActive;
        //     this.active5 = data.content.data.levelFive[0].totalActive;
        // });


    }

    getStockist() {
        this.service.getAllStokist(this.setState, this.setLg)
        .subscribe(
            (data: any) => {
                this.ref1Sort = [...data.content.data];
                this.ref1 = [...data.content.data];
              this.states = data.content.data;
            },
            (error) => {}
        );
    }

    onGetLg(e) {
        this.setState = e;
        this.getStockist();
        console.log(this.setState);
        this.service.getLg(e)
        .subscribe(
            (data: any) => {
              this.lg = data.content.data;
            },
            (error) => {}
        );
    }

    onGetStokist(e) {
        this.setLg = e;
        this.getStockist();
        console.log(e);
        this.service.getStokist(this.setState, this.setLg)
        .subscribe(
            (data: any) => {
              this.stokist = data.content.data;
            },
            (error) => {}
        );
    }

    updateFilter(event, num) {
        switch (num) {
            case 1:
                const val1 = event.target.value.toLowerCase();

                // filter our data
                const temp1 = this.ref1Sort.filter(function(d) {
                    // Change the column name here
                    // example d.places
                    return d.name.toLowerCase().indexOf(val1) !== -1 || !val1;
                });

                // update the rows
                this.ref1 = temp1;
                // Whenever the filter changes, always go back to the first page
                this.table.offset = 0;
                break;
            case 2:
                const val2 = event.target.value.toLowerCase();

                // filter our data
                const temp2 = this.ref2Sort.filter(function(d) {
                    // Change the column name here
                    // example d.places
                    return d.name.toLowerCase().indexOf(val2) !== -1 || !val2;
                });

                // update the rows
                this.ref2 = temp2;
                // Whenever the filter changes, always go back to the first page
                this.table2.offset = 0;
                break;
            case 3:
                const val3 = event.target.value.toLowerCase();

                // filter our data
                const temp3 = this.ref3Sort.filter(function(d) {
                    // Change the column name here
                    // example d.places
                    return d.area.toLowerCase().indexOf(val3) !== -1 || !val3;
                });

                // update the rows
                this.ref3 = temp3;
                // Whenever the filter changes, always go back to the first page
                this.table3.offset = 0;
                break;
            case 4:
                const val4 = event.target.value.toLowerCase();

                // filter our data
                const temp4 = this.ref4Sort.filter(function(d) {
                    // Change the column name here
                    // example d.places
                    return d.email.toLowerCase().indexOf(val4) !== -1 || !val4;
                });

                // update the rows
                this.ref4 = temp4;
                // Whenever the filter changes, always go back to the first page
                this.table4.offset = 0;
                break;
            case 5:
                const val5 = event.target.value.toLowerCase();

                // filter our data
                const temp5 = this.ref5Sort.filter(function(d) {
                    // Change the column name here
                    // example d.places
                    return d.fullname.toLowerCase().indexOf(val5) !== -1 || !val5;
                });

                // update the rows
                this.ref5 = temp5;
                // Whenever the filter changes, always go back to the first page
                this.table5.offset = 0;
                break;
        }
    }

}
