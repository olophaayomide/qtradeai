export class User {
    username?: string;
    password?: string;
    email?: string;
    fullname?: string;
    phone?: string;
    address?: string;
    birthday?: string;
    occupation?: string;
    parent?: string;
    profileImage?: string;
    state?: string;
    lg?: string;
    npassword?: string;
    opassword?: string;
    publicKey?: any;
    userID?: any;
    wpasss?: any;
    amount?: any;
    country?: string;
    region?: string;
}
