//Sample List
export class List {
    id: number;
    details: string;
    dateCreated: string;
    dp: string;
    dpRetina: string;
    creator: string;
    title: string;
    time: string;
    to:string[] =[];
  }
