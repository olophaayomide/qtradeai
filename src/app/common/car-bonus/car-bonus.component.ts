import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Component, Input, OnInit} from '@angular/core';
import {SessionStorageService} from 'ngx-webstorage';
import { ServicesClass } from '../../services/service';

@Component({
  selector: 'car-bonus',
  templateUrl: './car-bonus.component.html',
  styleUrls: ['./car-bonus.component.scss']
})
export class CarBonusComponent implements OnInit {
  _percentage: number = 0;
  _amount = '0';
  rewards;
  _width: string;
  balance;
  currency = this.SessionSt.retrieve('currency');

  constructor(public SessionSt: SessionStorageService, public service: ServicesClass, private http: HttpClient) { }

  ngOnInit() {
    this.service.getReward().subscribe((data) => {
      if (data) {
        this.rewards = data.content.data;
        // console.log(data);
      } else {}});

      this.http.get(this.service.rootUrl + '/customer/get/balance', {
        headers: new HttpHeaders()
            .set('Content-Type', 'application/json')
            .set('username', this.SessionSt.retrieve('username'))
            .set('publicKey', this.SessionSt.retrieve('publicKey'))
            .set('userID', this.SessionSt.retrieve('userid'))
            .set('region', this.SessionSt.retrieve('region'))
    }).subscribe((data: any = []) => {
        if (data.content != null || undefined) {
            this.balance = data.content.GEICarWallet;
            // console.log(this.balance.GEICarWallet)
            // return this.getBalance.push(balance);
        }
    });
  }

  ngAfterViewInit() {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    
  }

  @Input()
  set Amount(value: string){
    this._amount = value;
    const b = Number(value);
    this._percentage = b * 100 / 28000000;
    this._width = this._percentage.toString() + '%';
  }

    calc(e) {
        return 100 - e;
    }

    cSat1(x) {
      if (parseInt(x) >= parseInt(this.balance)) {
        return 'assets/img/cross.svg';
      } else {
        return 'assets/img/shield.svg';
      }
    }
  
    completed1() {
      if (this._amount === '0') {
        return 'assets/img/cross.svg';
      } else {
        return 'assets/img/shield.svg';
      }
    }
}
