import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarBonusComponent } from './car-bonus.component';

describe('CarBonusComponent', () => {
  let component: CarBonusComponent;
  let fixture: ComponentFixture<CarBonusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarBonusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarBonusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
