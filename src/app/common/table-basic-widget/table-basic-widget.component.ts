import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {DashboardComponent} from '../../dashboard/dashboard.component';
import {ServicesClass} from '../../services/service';
import {SessionStorageService} from 'ngx-webstorage';

@Component({
  selector: 'table-basic-widget',
  templateUrl: './table-basic-widget.component.html',
  styleUrls: ['./table-basic-widget.component.scss'],
  encapsulation:ViewEncapsulation.None
})
export class TableBasicWidgetComponent implements OnInit {
  data: any;
  data2: any;
  loading = false;
  details: any;
  currency = this.SessionSt.retrieve('currency');


  @Input()
  set BrokerPayload(value: any){
    this.data = value;
  }

  @Input()
  set ReferralPayload(value: any){
    this.data2 = value;
  }

  constructor(
      private dashboard: DashboardComponent,
      public service: ServicesClass,
      public SessionSt: SessionStorageService
  ) { }

  ngOnInit() {
    this.service.runDetails().then((data) => {
      if (data) {
        this.details = data;
      } else {}}
    ).catch(e => console.log(e));
  }

  async reload() {
    this.loading = true;
    this.dashboard.activities().then((data) => {});
    await this.service.runDetails().then((data) => {
      this.loading = false;
      if (data) {
        this.details = data;
      } else {}}
    ).catch(e => console.log(e));
    // return true;
  }

  verified(payload) {
    if (payload === '0') {
      return 'assets/img/cross.svg';
    } else {
      return 'assets/img/shield.svg';
    }
  }
}
