import {Component, Input, OnInit} from '@angular/core';
import {ServicesClass} from '../../services/service';

@Component({
  selector: 'quick-stats-widget',
  templateUrl: './quick-stats-widget.component.html',
  styleUrls: ['./quick-stats-widget.component.scss']
})
export class QuickStatsWidgetComponent implements OnInit {
  _order;
  details: any;

  constructor(
      public service: ServicesClass
  ) { }

  @Input()
  set Order(value: any){
    const result = value.find( ({ gei }) => gei === '1' );
    if (result && result.status !== '0') {
      this._order = result;
    } else {

    }

  }

  ngOnInit() {
    this.service.getOrders().subscribe((data) => {
      if (data) {
        const result = data.content.data.find(( {gei} ) => gei === '1');
        if (result && result.status !== '0') {
          this._order = result;
        } else {
          // this._gei = 0;
        }
      }
    });
  }

  dets() {
    if(this._order.gei === '1' && this._order.completed === '1') {
      return 'Trust Commitment Completed';
    } else if(this._order.gei === '1' && this._order.completed === '0') {
      return 'Trust Commitment not Fulfilled';
    } else if (this._order.gei === '0') {
      return 'No Current Commitment';
    }
  }

  cSat1() {
    if (this._order.cstat === '0') {
      return 'assets/img/cross.svg';
    } else {
      return 'assets/img/shield.svg';
    }
  }

  completed1() {
    if (this._order.completed === '0') {
      return 'assets/img/cross.svg';
    } else {
      return 'assets/img/shield.svg';
    }
  }
}
