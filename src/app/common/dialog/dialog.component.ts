import {AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {ModalDirective} from 'ngx-bootstrap/modal';
import {ServicesClass} from '../../services/service';
import {SessionStorageService} from 'ngx-webstorage';
import {NgForm} from '@angular/forms';
import {Router} from '@angular/router';



@Component({
    selector: 'app-dialog',
    templateUrl: './dialog.component.html',
    styleUrls: ['./dialog.component.scss'],
})
export class DialogComponent implements OnInit {
    @ViewChild('smStickUp') smStickUp: ModalDirective;
    @ViewChild('mdCreateBank') mdCreateBank: ModalDirective;
    @ViewChild('mdWithdrawal') mdWithdrawal: ModalDirective;
    @ViewChild('btcForm') d1:ElementRef;
    rootUrl = 'https://apis.qtradeai.com/public/api';


    header;
    public cont;
    public data;
    body;
    options = this.SessionSt.retrieve('banks');
    myBanks = this.SessionSt.retrieve('myBanks');
    bankName;
    accountNumber: any;
    accountName: any;
    Wpassword: any;
    WbankName: any;
    Wamount: any;
    Tamount: any;
    Tbene: any;
    Tpassword: any;
    selectedPayment: any;
    selectedPayment2: any;
    paymentID: any;
    amountf: any;
    btcID: any;
    packageName: any;
    WType: any;
    accountNameT: any;
    bankNameT: any;
    accountNumberT: any;
    paymentIDT: any;
    amountT: any;
    currencyT: any;
    methodType = [
        {value: 1, label: 'Bank Account'},
        {value: 2, label: 'Master Card'},
        {value: 3, label: 'BTC'},
        {value: 4, label: 'BMC'},
        {value: 5, label: 'BMCT'},
        {value: 6, label: 'GEI Product wallet'},
    ];
    paymentMethod = [
        {value: 1, label: 'Wallet'},
        {value: 2, label: 'Card'},
        {value: 3, label: 'Transfer'},
        {value: 4, label: 'BTC'},
    ];
    orderAddress: any;
    orderMethod: any;
    currentModal: any;
    pan: any;
    otp: any;
    transferDetails: any;
    code: any;
    btcForm: any;
    BVN: any;
    states = [
        { id: 'disabled', name: 'No Data', disabled: true }
    ];
    setState: any;
    lg = [
        { id: 'disabled', name: 'No Data', disabled: true }
    ];
    setLg: any;
    setArea: any;
    area = [
        { id: 'disabled', name: 'No Data', disabled: true }
    ];
    setStokist: any;
    stokist = [
        { id: 'disabled', name: 'No Data', disabled: true }
    ];



    constructor(
        public service: ServicesClass,
        public SessionSt: SessionStorageService,
        public router: Router
    ) {}

    ngOnInit() {
        this.service.getState()
        .subscribe(
            (data: any) => {
              this.states = data.content.data;
            },
            (error) => {}
        );
    }

    onGetLg(e) {
        this.lg = [];
        this.setState = e;
        console.log(this.setState);
        this.service.getLg(e)
        .subscribe(
            (data: any) => {
              this.lg = data.content.data;
            },
            (error) => {}
        );
    }

    onGetAreaCode(e) {
        this.setLg = e;
        console.log(e);
        this.service.getAreaCode(this.setState, e)
        .subscribe(
            (data: any) => {
              this.area = data.content.data;
            },
            (error) => {}
        );
    }

    onGetStokist(e) {
        this.setLg = e;
        console.log(e);
        this.service.getStokist(this.setState, this.setLg)
        .subscribe(
            (data: any) => {
              this.stokist = data.content.data;
            },
            (error) => {}
        );
    }

    close() {
        const q = document.getElementById(`${localStorage.getItem('currentModal')}1`);
        q.click();
        this.SessionSt.clear('modal');
    }

    closeDialog() {
        const q = document.getElementById('smallAlert1');
        q.click();
    }

    showDialog() {
        const q = document.getElementById('smallAlert');
        q.click();
    }

    showModal(e) {
        localStorage.setItem('currentModal', e);
        const q = document.getElementById(e);
        q.click();
    }

    deleteOrder(e) {
        this.service.cancelOrder(e)
            .subscribe((data: any = []) => {
                // console.log(data)
                if (data.error.status === '0') {
                    this.service.toast('success', 'Order Deleted Successful', 'Success');
                    location.reload();
                }else {
                    this.close();
                    this.service.toast('danger', data.error.message, 'Failed');
                }
            });
    }

    cancelPayment(e) {
        this.service.cancelPayment(e)
            .subscribe((data: any = []) => {
                // console.log(data)
                if (data.error.status === '0') {
                    this.service.toast('success', 'Payment Deleted Successful', 'Success');
                    location.reload();
                }else {
                    this.close();
                    this.service.toast('danger', data.error.message, 'Failed');
                }
            });
    }

    Pic(payment) {
        this.service.uploadproof(payment).subscribe((data: any = []) => {
            if (data.error.status === '0') {
                this.service.toast('success', 'Proof Updated Successful', 'Success');
                this.SessionSt.clear('modal');
                // location.reload();
                this.closeDialog();
            }else {
                this.SessionSt.clear('modal');
                this.service.toast('danger', data.error.message, 'Failed');
            }
        });
    }

    CreateBank(payload) {
        console.log(payload.value)
        this.service.createBank(payload.value)
            .subscribe((data: any = []) => {
                if (data.error.status === '0') {
                    this.service.toast('success', 'Bank Added Successfully', 'Success');
                    this.SessionSt.store('bank', '1');
                    location.reload();
                }else {
                    this.close();
                    this.service.toast('danger', data.error.message, 'Failed');
                }
            });
    }

    Withdraw(payload: NgForm) {
        if (payload.value.WTypeWType === 6) {
            this.service.withdraw2(payload.value)
            .subscribe((data: any = []) => {
                this.close();
                if (data.error.status === '0') {
                    this.service.toast('success', data.success.message, 'Success');
                    this.code = data.error.message;
                    this.showModal('claimCode');
                    payload.reset();
                }else {
                    this.service.toast('danger', data.error.message, 'Failed');
                }
            });
        } else {
            this.service.withdraw(payload.value)
                .subscribe((data: any = []) => {
                    this.close();
                    if (data.error.status === '0') {
                        this.service.toast('success', data.success.message, 'Success');
                        payload.reset();
                    }else {
                        this.service.toast('danger', data.error.message, 'Failed');
                    }
                });
        }
    }

    OrderCard( orderCardForm: NgForm ) {
        const e = orderCardForm.value.orderMethod.toString();
        this.service.orderCard(orderCardForm.value)
            .subscribe((data: any = []) => {
                this.close();
                if (data.error.status === '0') {
                    this.service.toast('success', data.success.message, 'Success');
                    this.SessionSt.store('packageName', 'Card Payment');
                    this.paymentContinue(e, {data: data.content});
                    orderCardForm.reset();
                }else {
                    this.service.toast('danger', data.error.message, 'Failed');
                }
            });
    }

    VerifyCard( verifyCardForm: NgForm ) {
        this.service.verifyCard(verifyCardForm.value)
            .subscribe((data: any = []) => {
                this.close();
                if (data.error.status === '0') {
                    this.service.toast('success', 'your Card has been verified', 'Success');
                    verifyCardForm.reset();
                }else {
                    this.service.toast('danger', data.error.message, 'Failed');
                }
            });
    }

    Transfer(payload: NgForm) {
        this.service.transfer(payload.value)
            .subscribe((data: any = []) => {
                this.close();
                if (data.error.status === '0') {
                    this.service.toast('success', 'Transfer Successful.', 'Success');
                    payload.reset();
                }else {
                    this.service.toast('danger', data.error.message, 'Failed');
                }
            });
    }

    continuePayment(e) {
        const q = this.SessionSt.retrieve('modal');
        const payload = {...q.q, method: e};
        this.close();
        this.service.subscribeMethod(payload)
            .subscribe((data: any = []) => {
                this.close();
                if (data.error.status === '0') {
                    // this.service.toast('success', data.success.message, 'Success');
                    this.paymentContinue(e, {data: data.content, payload: payload});
                }else {
                    this.service.toast('danger', data.error.message, 'Failed');
                }
            });
    }

    paymentContinue(e, payload) {
        const newPayload = {...payload.payload, currency: payload.data.account.currency};
        if (e === '1') {
        } else if (e === '2') {
            sessionStorage.setItem('pmr_', JSON.stringify(payload.data));
            location.replace('https://dashboard.qtradeai.com/payment');
        } else if (e === '3') {
            this.accountNameT = payload.data.account.accountName;
            this.accountNumberT = payload.data.account.accountNumber;
            this.paymentIDT = payload.data.paymentID;
            this.bankNameT = payload.data.account.bankName;
            this.amountT = payload.data.amount;
            this.currencyT = payload.data.account.currency;
            this.service.toast('success', 'Payment Ticket has been raised!', 'Success');
            this.showModal('walletTransfer');

        } else if (e === '4') {
            this.btcForm = payload.data.account.BTCpayForm;
            this.d1.nativeElement.insertAdjacentHTML('beforeend', this.btcForm);
            this.paymentID = payload.data.paymentID;
            this.amountf = payload.data.account.USDAmount;
            this.btcID = payload.orderID;
            this.packageName = this.SessionSt.retrieve('packageName');
            this.showModal('btcPayment');
        }
    }

    commitment( e ) {
        const q = this.SessionSt.retrieve('modal');
        const f = q.e;
        const payload = {orderID: f.orderID, method: e};
        this.close();
        this.service.commitmentFee1(payload)
            .subscribe((data: any = []) => {
                if (data.error.status === '0') {
                    // this.service.toast('success', data.success.message, 'Success');
                    this.paymentContinue(e, {data: data.content, payload: payload});
                }else {
                    this.service.toast('danger', data.error.message, 'Failed');
                }
            });
    }

    CardLimit( CardLimitForm: NgForm ) {
        this.service.CardLimit(CardLimitForm.value)
            .subscribe((data: any = []) => {
                this.close();
                if (data.error.status === '0') {
                    this.service.toast('success', data.succes.message, 'Success');
                    CardLimitForm.reset();
                }else {
                    this.service.toast('danger', data.error.message, 'Failed');
                }
            });
    }

    modalSessionClear() {
        this.SessionSt.clear('modal');
    }

    continue() {
        const ret = this.SessionSt.retrieve('modal');
        switch (ret.h) {
            case 'delOrder':
                console.log(ret);
                this.deleteOrder(ret.e);
                break;
            case 'cancelPayment':
                this.cancelPayment(ret.e);
                break;
            case 'uploadPayment':
                this.Pic(ret.e);
                // location.reload();
                break;
            case 'commitment':
                this.commitment(ret.e);
                break;
            case 'delPayment':
                this.cancelPayment(ret.e);
                break;
            default:
                console.log('fjn');
        }
    }

    gotToLog() {
        this.close();
        this.router.navigateByUrl('/app/logs');
    }
}
