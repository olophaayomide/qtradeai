import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, Route } from '@angular/router';
import { Observable } from 'rxjs';
import {ServicesClass} from './service';

@Injectable()
export class SessionGuard implements CanActivate {


    constructor(private _authService: ServicesClass, private _router: Router) {
    }

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        if (!this._authService.isAuthenticated()) {
            return true;
        }

        // navigate to login page
        this._router.navigate(['/app']);
        // you can save redirect url so after anything we can move them back to the page they requested
        return false;
    }

}
