import {User} from '../model/user.model';
import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {MessageService} from '../@pages/components/message/message.service';
import {SessionStorageService} from 'ngx-webstorage';
import {Router} from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class ServicesClass {
     rootUrl = 'https://apis.qtradeai.com/public/api';
     picUrl = 'https://apis.qtradeai.com/public/';
     currency = this.SessionSt.retrieve('currency');
     self = this.SessionSt.retrieve('currency');
     Orders1: any = [];
     Withdrawal1: any = [];
     Transfers1: any = [];
     Payment1: any = [];
     getSelf: any = [];
     getBalance: any = [];
     Numbers: any = [];

    constructor(
        private http: HttpClient,
        private _notification: MessageService,
        private SessionSt: SessionStorageService,
        private router: Router
        ) {}

    getRegion = function() {
        return this.http.get(this.rootUrl + '/get/regions');
    };

    getState = function() {
        return this.http.get(this.rootUrl + '/get/states');
    };

    getLg = function(id) {
        return this.http.get(this.rootUrl + '/get/lgs/' + id);
    };

    getAreaCode = function(state, lg) {
        return this.http.get(this.rootUrl + '/customer/get/area/-/' + state + '/' + lg);
    };

    getStokist = function(state, lg) {
        return this.http.get(this.rootUrl + '/get/partner/' + state + '/' + lg + '/-/-');
    };

    getAllStokist = function(state, lg) {
        return this.http.get(this.rootUrl + '/get/partner/' + state + '/' + lg + '/-/-');
    };

    loginUser = function(user: User) {
        const body = {
            username: user.username,
            password: user.password,
            region: user.region
        };
        return this.http.post(this.rootUrl + '/customer/login',  {'data': body});
    };

    registerUser = function(user) {
        const body = {
            username: user.username,
            password: user.password,
            email: user.email,
            fullname: user.fullname,
            parent: user.parent,
            phone: user.phone,
        };
        return this.http.post(this.rootUrl + '/customer/create/self', {'data': body});
    };

    forgot = function(user) {
        const body = {
            email: user.email,
            username: user.username
        };
        return this.http.post(this.rootUrl + '/customer/retrieve/password/1',  {'data': body});
    };

    updateSelf = function(payload) {
        return this.http.put(this.rootUrl + '/customer/update/self', {'data': payload});
    };

    updateKYC = function(payload) {
        const body = {
            countryID: payload.countryID,
            addressID: payload.addressID,
            userID: this.SessionSt.retrieve('userid'),
            publicKey: this.SessionSt.retrieve('publicKey'),
            username: this.SessionSt.retrieve('username')
        };
        // console.log(body);
        return this.http.post(this.rootUrl + '/customer/create/kyc', {'data': body});
    };

    updatePic = function(payload){
        const body = {
            profileImage: payload,
            username: this.SessionSt.retrieve('username'),
            userID: this.SessionSt.retrieve('userid'),
            publicKey: this.SessionSt.retrieve('publicKey')
        };
        // console.log(body);
        return this.http.put(this.rootUrl + '/customer/update/profileImage', {'data': body});
    };

    runDetails = async function() {
        this.Numbers = [];

        this.http.get(this.rootUrl + '/customer/get/orders/0/9898989898989898', {
            headers: new HttpHeaders()
                .set('Content-Type', 'application/json')
                .set('username', this.SessionSt.retrieve('username'))
                .set('publicKey', this.SessionSt.retrieve('publicKey'))
                .set('userID', this.SessionSt.retrieve('userid'))
        }).subscribe(async ( data: any = [] ) => {
            if (data.content != null || undefined) {
                const orders = data.content.data;
                const ordersL = data.content.data.length;
                return await this.Orders1.push(orders, ordersL);
            }
        });

        await this.http.get(this.rootUrl + '/customer/get/withdrawal/0/9898989898989898', {
            headers: new HttpHeaders()
                .set('Content-Type', 'application/json')
                .set('username', this.SessionSt.retrieve('username'))
                .set('publicKey', this.SessionSt.retrieve('publicKey'))
                .set('userID', this.SessionSt.retrieve('userid'))
        }).subscribe((data: any = []) => {
            if (data.content != null || undefined) {
                const withdraw = data.content.data;
                const withdrawL = data.content.data.length;
                return this.Withdrawal1.push(withdraw, withdrawL);
            }
        });

        await this.http.get(this.rootUrl + '/customer/get/transfers/0/9898989898989898', {
            headers: new HttpHeaders()
                .set('Content-Type', 'application/json')
                .set('username', this.SessionSt.retrieve('username'))
                .set('publicKey', this.SessionSt.retrieve('publicKey'))
                .set('userID', this.SessionSt.retrieve('userid'))
        }).subscribe((data: any = []) => {
            if (data.content != null || undefined) {
                const transfers = data.content.data;
                const transfersL = data.content.data.length;
                return this.Transfers1.push(transfers, transfersL);
            }
        });

        await this.http.get(this.rootUrl + '/customer/get/payments/0/9898989898989898', {
            headers: new HttpHeaders()
                .set('Content-Type', 'application/json')
                .set('username', this.SessionSt.retrieve('username'))
                .set('publicKey', this.SessionSt.retrieve('publicKey'))
                .set('userID', this.SessionSt.retrieve('userid'))
                .set('region', this.SessionSt.retrieve('region'))
        }).subscribe((data: any = []) => {
            if (data.content != null || undefined) {
                const payments = data.content.data;
                const paymentsL = data.content.data.length;
                return this.Payment1.push(payments, paymentsL);
            }
        });

        await this.http.get(this.rootUrl + '/customer/get/self', {
            headers: new HttpHeaders()
                .set('Content-Type', 'application/json')
                .set('username', this.SessionSt.retrieve('username'))
                .set('publicKey', this.SessionSt.retrieve('publicKey'))
                .set('userID', this.SessionSt.retrieve('userid'))
                .set('userID', this.SessionSt.retrieve('userid'))

        }).subscribe((data: any = []) => {
            if (data.content != null || undefined) {
                this.totalWithdrawal = data.content.data[0].totalWithdrawal;
                this.totalSubscription = data.content.data[0].totalSubscription;
                this.Id = data.content.data[0].userID;
                this.SessionSt.store('userData', data.content.data[0]);
                return this.getSelf.push(data.content.data[0]);
            }
        });

        await this.http.get(this.rootUrl + '/customer/get/balance', {
            headers: new HttpHeaders()
                .set('Content-Type', 'application/json')
                .set('username', this.SessionSt.retrieve('username'))
                .set('publicKey', this.SessionSt.retrieve('publicKey'))
                .set('userID', this.SessionSt.retrieve('userid'))
                .set('region', this.SessionSt.retrieve('region'))
        }).subscribe((data: any = []) => {
            if (data.content != null || undefined) {
                const balance = data.content;
                return this.getBalance.push(balance);
            }
        });

        this.http.get(this.rootUrl + '/get/banks')
            .subscribe((data: any = []) => {
                this.SessionSt.store('banks', data.content.data);
            });

        this.Numbers = [];
        await this.Numbers.push(
            {'orders': this.Orders1},
            {'withdrawal': this.Withdrawal1},
            {'transfer': this.Transfers1},
            {'payment': this.Payment1},
            {'getSelf': this.getSelf},
            {'getBalance': this.getBalance},
            );
        return this.Numbers;
    };

    cancelPayment = function(payment) {
        const body = {
            'status': '-1',
            'orderID': payment.orderID,
            'paymentID': payment._id,
            'userID': this.SessionSt.retrieve('userid'),
            'publicKey': this.SessionSt.retrieve('publicKey'),
            'username': this.SessionSt.retrieve('username')
        };
        // console.log(body);
        return this.http.put(this.rootUrl + '/customer/verify/payment', {'data': body});
    };

    cancelOrder = function(order) {
        const body = {
            'status': '-1',
            'orderID': order.orderID,
            'userID': this.SessionSt.retrieve('userid'),
            'publicKey': this.SessionSt.retrieve('publicKey'),
            'username': this.SessionSt.retrieve('username')
        };
        // console.log(body);
        return this.http.put(this.rootUrl + '/customer/update/order', {'data': body});
    };

    uploadproof = function(payment){
        const body = {
            pop: payment.b,
            orderID: payment.a.orderID,
            paymentID: payment.a._id,
            status: '1',
            username: this.SessionSt.retrieve('username'),
            userID: this.SessionSt.retrieve('userid'),
            publicKey: this.SessionSt.retrieve('publicKey')
        };
        // console.log(body);
        return this.http.put(this.rootUrl + '/customer/verify/payment', {'data': body});
    };

    createBank = function(payload) {
        const body = {
            bankName: payload.bankName,
            accountNumber: payload.accountNumber,
            accountName: payload.accountName,
            swiftCode: payload.swiftCode,
            sortCode: payload.sortCode,
            bankAddress: payload.bankAddress,
            username: this.SessionSt.retrieve('username'),
            userID: this.SessionSt.retrieve('userid'),
            publicKey: this.SessionSt.retrieve('publicKey')

        };
        return this.http.post(this.rootUrl + '/customer/create/bank', {'data': body});
    };

    withdraw = function(payload) {
        const body = {
            bankID: payload.WbankName,
            amount: payload.Wamount,
            password: payload.Wpassword,
            method: payload.WTypeWType,
            stockist: payload.setStokist,
            username: this.SessionSt.retrieve('username'),
            userID: this.SessionSt.retrieve('userid'),
            publicKey: this.SessionSt.retrieve('publicKey')

        };
        return this.http.post(this.rootUrl + '/customer/create/withdrawal', {'data': body});
    };

    withdraw2 = function(payload) {
        const body = {
            amount: payload.Wamount,
            password: payload.Wpassword,
            stockist: payload.setStokist,
            username: this.SessionSt.retrieve('username'),
            userID: this.SessionSt.retrieve('userid'),
            publicKey: this.SessionSt.retrieve('publicKey')

        };
        return this.http.post(this.rootUrl + '/customer/create/withdrawal/2', {'data': body});
    };

    orderCard = function(payload) {
        const body = {
            address: payload.address,
            method: payload.orderMethod,
            username: this.SessionSt.retrieve('username'),
            userID: this.SessionSt.retrieve('userid'),
            publicKey: this.SessionSt.retrieve('publicKey')

        };
        return this.http.post(this.rootUrl + '/customer/order/card', {'data': body});
    };

    verifyCard = function(payload) {
        const body = {
            otp: payload.otp,
            pan: payload.pan,
            username: this.SessionSt.retrieve('username'),
            userID: this.SessionSt.retrieve('userid'),
            publicKey: this.SessionSt.retrieve('publicKey')

        };
        return this.http.put(this.rootUrl + '/customer/update/card-order', {'data': body});
    };

    CardLimit = function(payload) {
        const body = {
            bvn: payload,
            username: this.SessionSt.retrieve('username'),
            userID: this.SessionSt.retrieve('userid'),
            publicKey: this.SessionSt.retrieve('publicKey')

        };
        return this.http.put(this.rootUrl + '/customer/update/card-order', {'data': body});
    };

    transfer = function(payload) {
        const body = {
            beneficiary: payload.Tbene,
            amount: payload.Tamount,
            password: payload.Tpassword,
            username: this.SessionSt.retrieve('username'),
            userID: this.SessionSt.retrieve('userid'),
            publicKey: this.SessionSt.retrieve('publicKey')
        };
        // console.log(body)
        return this.http.post(this.rootUrl + '/customer/create/transfer', {'data': body});
    };

    subscribeOrder = function(payload) {
        const body = {
            amount: payload.calculatedPrice,
            count: '1',
            package: payload.id,
            username: this.SessionSt.retrieve('username'),
            userID: this.SessionSt.retrieve('userid'),
            publicKey: this.SessionSt.retrieve('publicKey')

        };
        return this.http.post(this.rootUrl + '/customer/create/order', {'data': body});
    };

    subscribeMethod = function(payload) {
        const body = {
            amount: payload.amount,
            method: payload.method,
            orderID: payload.orderID,
            currency: this.SessionSt.retrieve('currency'),
            username: this.SessionSt.retrieve('username'),
            userID: this.SessionSt.retrieve('userid'),
            publicKey: this.SessionSt.retrieve('publicKey')
        };
        return this.http.post(this.rootUrl + '/customer/create/payment', {'data': body});
    };

    commitmentFee1 = function(payload) {
        const body = {
            method: payload.method,
            orderID: payload.orderID,
            username: this.SessionSt.retrieve('username'),
            userID: this.SessionSt.retrieve('userid'),
            publicKey: this.SessionSt.retrieve('publicKey')
        };
        return this.http.post(this.rootUrl + '/customer/pay-commitment-fee', {'data': body});
    };

    changePassword = function(payload) {
        const body = {
            oldPassword: payload.oldPassword,
            newPassword: payload.newPassword,
            username: this.SessionSt.retrieve('username'),
            userID: this.SessionSt.retrieve('userid'),
            publicKey: this.SessionSt.retrieve('publicKey')
        };
        return this.http.post(this.rootUrl + '/customer/update/password', {'data': body});
    };

    getBroker = function() {
        return this.http.post(this.rootUrl + '/customer/get/broker');
    };

    getPackages = function() {
        return this.http.get(this.rootUrl + '/get/packages');
    };

    getBeepEstate = function() {
        return this.http.get(this.rootUrl + '/get/beepestate-packages');
    };

    getMyBank = function() {
        return this.http.get(this.rootUrl + '/customer/get/bank');
    };

    getPromotionalMaterials = function() {
        return this.http.get(this.rootUrl + '/customer/get/promotional-material/0/9898989898989898');
    };

    getOrders = function() {
        return this.http.get(this.rootUrl + '/customer/get/orders/0/9898989898989898');
    };

    getWithdrawal = function() {
        return this.http.get(this.rootUrl + '/customer/get/withdrawal/0/9898989898989898');
    };

    getTransfers = function() {
        return this.http.get(this.rootUrl + '/customer/get/transfers/0/9898989898989898');
    };

    getPayment = function() {
        return this.http.get(this.rootUrl + '/customer/get/payments/0/9898989898989898');
    };

    getReward = function() {
        return this.http.get(this.rootUrl + '/get/rewards');
    };

    // getCommission = function() {
    //     return this.http.get(this.rootUrl + '/customer/get/commission/0/9898989898989898');
    // };

    getCards = function() {
        return this.http.get(this.rootUrl + '/customer/get/cards/0/9898989898989898');
    };

    getReferrals = function() {
        return this.http.get(this.rootUrl + '/customer/get/referrals');
    };

    getActiveReferrals = function() {
        return this.http.get(this.rootUrl + '/customer/get/active/referrals/count');
    };

    isAuthenticated() {
        return !!this.SessionSt.retrieve('userData');
    }

    logout() {
        this.SessionSt.clear('userData');
        this.router.navigate(['/auth/session/login']);
    }

    toast = function(type, message, title) {
        this._notification.create(
            type,
            message,
            {
                Title: title,
                imgURL: null,
                Position: 'top-right',
                Style: 'flip-bar',
                Duration: 9000
            });
    };
}
