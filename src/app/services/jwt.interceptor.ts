import { Injectable } from '@angular/core';
import {HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse, HttpErrorResponse} from '@angular/common/http';
// tslint:disable-next-line:import-blacklist
import { Observable, of } from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import { SessionStorageService} from 'ngx-webstorage';
import {tap, catchError} from 'rxjs/operators';
import {ServicesClass} from './service';


@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  public token: string;
  parent: string;
    constructor(private router: Router, private SessionSt: SessionStorageService, public service: ServicesClass, public route: ActivatedRoute) {}

  private getToken(): string {
    if (!this.token) {
      this.token = this.SessionSt.retrieve('region');
    }
    return this.token;
  }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        // const currentUser = this.authenticationService.currentUserValue;
        const url = this.router.url;
        // @ts-ignore
        if (
            url === '/auth/session/login' ||
            url === '/auth/session/forgot-password' ||
            url === '/auth/session/register/' + url.replace('/auth/session/register/', '') ||
            url === '/auth/session/register') {
        } else {
            request = request.clone({
                setHeaders: {
                    region: this.SessionSt.retrieve('region'),
                    username: this.SessionSt.retrieve('username'),
                    userID: this.SessionSt.retrieve('userid'),
                    publicKey: this.SessionSt.retrieve('publicKey'),
                }
            });
        }

        return next.handle(request).pipe(
            tap(evt => {
                if (evt instanceof HttpResponse) {
                    if (evt.status === 200) {
                        if (evt.body.error.message === 'The profile have not been found') {
                            // this.router.navigateByUrl('/auth/session/login');
                            this.SessionSt.clear('userData');
                            this.router.navigate(['/auth/session/login']);
                        }
                    }
                }
            }),
            // REMEMBER TO TAKE THIS OUT!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            catchError((err: any) => {
                this.service.toast('danger', 'There was an error with your request!', 'Failed');
                if (err instanceof HttpErrorResponse) {
                    try {
                        // this.service.toast('danger', 'There was an error with your request!', 'Failed');
                    } catch (e) {
                        // this.service.toast('danger', 'There was an error with your request!', 'Failed');
                    }
                    // log error
                }
                return of(err);
            }));

    }
}
