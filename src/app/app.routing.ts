import { Routes } from '@angular/router';
// Layouts
import {
    BlankComponent,
    Home
} from './@pages/layouts';
import {LogsComponent} from './logs/logs.component';
import {AccountComponent} from './account/account.component';
import {PackagesComponent} from './packages/packages.component';
import {NewsComponent} from './news/news.component';
import {AuthGuard} from './services/authGuard';
import {DashboardComponent} from './dashboard/dashboard.component';
import {GeiPackagesComponent} from './geipackages/gei-packages.component';
import {ReferralComponent} from './referrals/referral.component';
import {ProfileComponent} from './profile/profile.component';
import { StokistComponent } from './stokists/stokists.component';
import { BeepEstateComponent } from './beepestate/beepestate.component';


export const AppRoutes: Routes = [

    {
        path: '',
        redirectTo: 'auth/session/login',
        pathMatch: 'full'
    },
    {
        path: 'auth',
        component: BlankComponent,
        children: [{
            path: 'session',
            loadChildren: './session/session.module#SessionModule'
        }]
    },

  {
    path: 'app',
    component: Home,
    canActivate: [AuthGuard],
    children: [{
        path: '',
        component: DashboardComponent
    }, {
        path: 'support-packages',
        component: PackagesComponent
    }, {
        path: 'gei-packages',
        component: GeiPackagesComponent
    }, {
        path: 'beepestate',
        component: BeepEstateComponent
    }, {
        path: 'logs',
        component: LogsComponent
    }, {
        path: 'referrals',
        component: ReferralComponent
    }, {
        path: 'stockist',
        component: StokistComponent
    }, {
        path: 'account',
        component: AccountComponent
    }, {
        path: 'profile',
        component: ProfileComponent
    }, {
        path: 'news',
        component: NewsComponent
    }
    ]
  },

  {
    path: '**',
    redirectTo: 'auth/session/error/404'
  },
];
