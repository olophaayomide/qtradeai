import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {ServicesClass} from '../../services/service';
import {Router} from '@angular/router';
import {SessionStorageService} from 'ngx-webstorage';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-register-page',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterPageComponent implements OnInit {
  // Sample Variables for the form
  txtfname;
  txtphone;
  txtusername;
  txtpassword;
  txtemail;
  check;
  parent: any;
  ref = this.SessionSt.store('ref', 'bmcoin');

  constructor(
      private service: ServicesClass,
      private router: Router,
      public SessionSt: SessionStorageService,
      public route: ActivatedRoute
  ) { }

  ngOnInit() {
    if (this.route.snapshot.paramMap.get('username') !== null) {
      this.parent = this.route.snapshot.paramMap.get('username');
    } else {
      this.parent = '';
    }
  }

  register(form: NgForm) {
    let q = {...form.value, parent: this.parent};
    this.service.registerUser(q)
        .subscribe((data) => {
          if (data.error.status === '0') {
            form.reset();
            this.service.toast('success', 'Registration successful,  kindly check Your email / spam to verify Your Account', 'Successful');
            setTimeout(
                () => this.router.navigateByUrl('auth/session/login')
                , 3000);

          } else {
            this.service.toast('danger', data.error.message, 'Failed');
          }
        });
  }

}
