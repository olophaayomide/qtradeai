import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {SessionStorageService} from 'ngx-webstorage';
import {ServicesClass} from '../../services/service';
import {DialogComponent} from '../../common/dialog/dialog.component';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  // Sample Varriables
  userName;
  password;
  options = [
    { value: 'africa', label: 'Africa' },
    { value: 'america', label: 'America' },
    { value: 'europe', label: 'Europe' },
  ];
  selectedOption = 'Africa';
  regions: any;
  constructor(
      private router: Router,
      private SessionSt: SessionStorageService,
      public service: ServicesClass,
      private dialog: DialogComponent
      ) { }

  ngOnInit() {
    this.service.getRegion()
        .subscribe(
            (data: any) => {
              this.regions = data.content.data;
                this.SessionSt.store('currency', 'NGN');
            },
            (error) => {}
        );
  }

  login(form) {
    if (form.value.region === undefined) {
      this.service.toast('danger', 'please select region', 'Failed');
    } else {
      this.service.loginUser(form.value)
          .subscribe((data: any = []) => {
            if (data.error.status === '0') {
              this.SessionSt.store('username', form.value.username);
              this.SessionSt.store('publicKey', data.content.data[0].publicKey);
              this.SessionSt.store('userID', data.content.data[0].userID);
              this.SessionSt.store('region', form.value.region);
              // this.SessionSt.store('address', data.content.data[0].address);
              // this.SessionSt.store('occupation', data.content.data[0].occupation);
              // this.SessionSt.store('fullname', data.content.data[0].fullname);
              // this.SessionSt.store('phone', data.content.data[0].phone);
              // this.SessionSt.store('birthday', data.content.data[0].birthday);
              // this.SessionSt.store('email', data.content.data[0].email);
              // this.SessionSt.store('state', data.content.data[0].state);
              // this.SessionSt.store('lg', data.content.data[0].lg);
              // this.SessionSt.store('dateJoined', data.content.data[0].dateJoined);
              this.SessionSt.store('profileimage', this.service.picUrl + '/' + data.content.data[0].profile);
              // this.toastrService.success('Login Successful', 'Successful', {
              //   timeOut: 3000,
              // });
              this.SessionSt.store('regionData', form.value.region);
              this.SessionSt.store('userData', data.content.data[0]);
              this.router.navigateByUrl('/app');
            } else {
              this.service.toast('danger', data.error.message, 'Failed');
            }
          });
    }
  }

    forgot(e) {
        this.dialog.showDialog();
    }

  saveRegion(e) {
      const result = this.regions.find( ({ region }) => region === e );
      this.SessionSt.store('currency', result.name);
  }
}
