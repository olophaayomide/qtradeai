import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import {ServicesClass} from '../../services/service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  email: any;
  username: any;

  constructor(
      private service: ServicesClass,
      private router: Router,
  ) { }

  ngOnInit() {
  }

  forgot(form: NgForm) {
      // console.log(form.value);
    this.service.forgot(form.value)
        .subscribe((data) => {
          if (data.error.status === '') {
            form.reset();
            this.service.toast('success', 'Check your E-mail for confirmation', 'Success');
            this.router.navigateByUrl('auth/session/login');
          } else {
            this.service.toast('danger', data.error.message, 'Failed');
          }
        });
  }

}
