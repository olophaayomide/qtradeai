import { Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterPageComponent } from './register/register.component';
import { ErrorComponent } from './error/error.component';
import {ForgotPasswordComponent} from './forgotPassword/forgot.component';
import {SessionGuard} from '../services/sessionGuard';

export const SessionRoute: Routes = [
  {
    path: '',
    children: [{
      path: 'login',
      canActivate: [SessionGuard],
      component: LoginComponent
    },{
      path: 'register',
      canActivate: [SessionGuard],
      component: RegisterPageComponent
    }, {
      path: 'register/:username',
      canActivate: [SessionGuard],
      component: RegisterPageComponent
    },{
      path: 'error/:type',
      canActivate: [SessionGuard],
      component: ErrorComponent
    },{
      path: 'forgot-password',
      canActivate: [SessionGuard],
      component: ForgotPasswordComponent
    }]
  }
];
