import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SessionRoute } from './session.routing';

import { SharedModule } from '../@pages/components/shared.module';
import { ErrorComponent } from './error/error.component';
import { LoginComponent } from './login/login.component';
import { RegisterPageComponent } from './register/register.component';
import {pgSelectModule} from '../@pages/components/select/select.module';
import {ForgotPasswordComponent} from './forgotPassword/forgot.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule, ReactiveFormsModule,
    SharedModule,
    RouterModule.forChild(SessionRoute), pgSelectModule,
  ],
  declarations: [ErrorComponent, LoginComponent, ForgotPasswordComponent, RegisterPageComponent]
})
export class SessionModule { }
